describe('Frontend IHM visit spec', () => {
    beforeEach('Connecting to the ticket booking page', () => {
        cy.visit('https://ticket-frontend-eight.vercel.app/');
    });
    

    it('Check connecting to the site and search result', () => {   
        cy.get("#departure").type("Paris");
        cy.get("#arrival").type("Bruxelles");       
        cy.get("#search").click();
        // Check result show
        cy.get("#results").should('exist');        
        cy.contains("Paris > Bruxelles");
    })

    it('Select ticket to book', () => {   
        cy.get("#departure").type("Bruxelles");
        cy.get("#arrival").type("Marseille");       
        cy.get("#search").click();
        // Check result of search and book
        cy.get("#results").should('exist');
        cy.get(".book").first().click();

        // Check cart show
        cy.contains("My cart");
        cy.contains("Purchase");
    })

    it('Select ticket to book and delete from cart', () => {   
        var cartLength = 0;
        cy.get('a[href="cart.html"]').click();
        cy.get('.delete').should((elements) => {
            cartLength = elements.length
            expect(elements.length).to.be.greaterThan(0);
        });

        cy.get('#title').click();
        cy.get("#departure").type("Paris");
        cy.get("#arrival").type("Bruxelles");       
        cy.get("#search").click();
        // Check result of search and book
        cy.get("#results").should('exist');
        cy.get(".book").first().click();

        // Check cart delete
        cy.contains("My cart");
        cy.contains("Purchase");
        cy.get('.delete').its('length').should('be.gte', cartLength)
        cy.get('.delete').first().click();

        cy.get('.xxxx').should('have.length', cartLength);
    })

    
    it('Check booking', () => {   
        cy.get('#title').click();
        cy.get("#departure").type("Paris");
        cy.get("#arrival").type("Bruxelles");       
        cy.get("#search").click();
        // Check result of search and book
        cy.get("#results").should('exist');
        cy.get(".book").first().click();

        cy.get("#purchase").click();
        cy.get('a[href="bookings.html"]').click();
    })

  })