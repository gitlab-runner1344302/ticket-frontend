module.exports = {
	e2e: {
		baseUrl: 'https://ticket-frontend-eight.vercel.app',
		specPattern: 'cypress/integration/*.spec.{js,jsx,ts,tsx}',
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},
}
